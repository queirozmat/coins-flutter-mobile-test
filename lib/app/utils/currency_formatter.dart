import 'package:decimal/decimal.dart';
import 'package:intl/intl.dart';

String currencyFormatter(String currency) {
  String currencyConverted;
  String defaultCurrency = currency;
  List<String> splitCurrency = defaultCurrency.split('.').last.split('');
  Decimal decimalCurrency = Decimal.parse(defaultCurrency);

  if (splitCurrency.length == 3) {
    int currencyInt = int.parse('${Decimal.parse("1000") * decimalCurrency}');
    currencyConverted =
        NumberFormat.simpleCurrency(locale: 'pt_BR').format(currencyInt);
  } else {
    double currencyDouble = double.parse(defaultCurrency);
    currencyConverted = NumberFormat.simpleCurrency(
      locale: 'pt_BR',
    ).format(currencyDouble);
  }
  return currencyConverted;
}
