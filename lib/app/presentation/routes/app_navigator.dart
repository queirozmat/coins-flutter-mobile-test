import 'package:get/get.dart';
import 'package:moedas/app/presentation/coins/coins_page.dart';

class AppRoutes {
  static const String coins = '/';
}

class AppPages {
  static final pages = [
    GetPage(
      name: AppRoutes.coins,
      page: () => const CoinsPage(),
    ),
  ];
}
