import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:moedas/app/domain/models/wallet_model.dart';

class CoinsRepository {
  final Dio client = Dio();

  Future<WalletModel> getWalletAndCoins() async {
    try {
      final response = await client.get(
          'https://gitlab.com/queirozmat/coins-flutter-mobile-test/-/raw/78279437fa9b5b2d48a44896d122bd418ac68ceb/criptomoedas.json');

      return WalletModel.fromJson(jsonDecode(response.data));
    } catch (e) {
      log(e.toString());
      rethrow;
    }
  }
}
