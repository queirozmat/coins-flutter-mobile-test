import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moedas/app/presentation/controllers/coin_controller.dart';
import 'package:moedas/app/utils/currency_formatter.dart';

class CoinsPage extends StatefulWidget {
  const CoinsPage({Key? key}) : super(key: key);

  @override
  State<CoinsPage> createState() => _CoinsPageState();
}

class _CoinsPageState extends State<CoinsPage> {
  final CoinController coinController = Get.put(CoinController());
  bool isLoading = true;
  bool valueViability = true;

  @override
  initState() {
    getCoins();
    super.initState();
  }

  Future<void> getCoins() async {
    setState((() => isLoading = true));
    await coinController.getWalletAndCoins();
    setState(() => isLoading = false);
  }

  void cahngeValueViability() {
    valueViability
        ? setState(() => valueViability = false)
        : setState(() => valueViability = true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading
          ? const Center(child: CircularProgressIndicator())
          : Stack(
              children: [
                Positioned(
                  top: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 0, 34, 61)),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.35,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.05,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              const Icon(
                                Icons.wallet,
                                color: Colors.white,
                              ),
                              const Text(
                                'Minha Carteira',
                                style: TextStyle(
                                    fontSize: 22, color: Colors.white),
                              ),
                              GestureDetector(
                                onTap: () => cahngeValueViability(),
                                child: valueViability
                                    ? const Icon(
                                        Icons.visibility_off,
                                        color: Colors.white,
                                      )
                                    : const Icon(
                                        Icons.visibility,
                                        color: Colors.white,
                                      ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.05),
                          child: const Text(
                            'Saldo na carteira:',
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              'R\$',
                              style:
                                  TextStyle(fontSize: 30, color: Colors.white),
                            ),
                            Text(
                              valueViability
                                  ? ' *********'
                                  : currencyFormatter(coinController
                                          .wallet.value.userBalance!)
                                      .split('\$')
                                      .last,
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 30,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.05,
                          ),
                          child: Text(
                            'ID da Carteira: ${coinController.wallet.value.walletId!}',
                            style: const TextStyle(
                                color: Colors.white, fontSize: 14),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.3,
                  child: Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.030,
                        bottom: MediaQuery.of(context).size.height * 0.1),
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Colors.blue,
                              Colors.white,
                            ]),
                        borderRadius: BorderRadius.only(
                            //topLeft: Radius.circular(30),
                            topRight: Radius.circular(30))),
                    child: SingleChildScrollView(
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.65,
                        //height: 550,
                        child: Obx(
                          (() {
                            return RefreshIndicator(
                              onRefresh: () =>
                                  coinController.getWalletAndCoins(),
                              child: ListView.separated(
                                  itemBuilder: ((context, index) {
                                    return GestureDetector(
                                      onTap: (() => showDialog(
                                            context: context,
                                            builder: (context) => AlertDialog(
                                              shape:
                                                  const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  15))),
                                              title: Text(
                                                  '${coinController.wallet.value.coins![index].currencyName}'
                                                  ' - ${coinController.wallet.value.coins![index].symbol}'),
                                              content: Text(coinController
                                                  .wallet
                                                  .value
                                                  .coins![index]
                                                  .details!
                                                  .about!),
                                            ),
                                          )),
                                      child: ListTile(
                                        leading: Image.network(coinController
                                            .wallet
                                            .value
                                            .coins![index]
                                            .imageUrl!),
                                        title: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(coinController.wallet.value
                                                .coins![index].currencyName!),
                                            Text(
                                              coinController.wallet.value
                                                  .coins![index].symbol!,
                                              style: const TextStyle(
                                                  color: Colors.black54),
                                            )
                                          ],
                                        ),
                                        trailing: Column(
                                          children: [
                                            Text(currencyFormatter(
                                                coinController.wallet.value
                                                    .coins![index].cotation!)),
                                          ],
                                        ),
                                      ),
                                    );
                                  }),
                                  separatorBuilder: (_, __) => const Divider(),
                                  itemCount: coinController
                                      .wallet.value.coins!.length),
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
