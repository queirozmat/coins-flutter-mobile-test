import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:moedas/app/presentation/routes/app_navigator.dart';
import 'package:moedas/main_binding.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: AppRoutes.coins,
      getPages: AppPages.pages,
      initialBinding: MainBinding(),
    );
  }
}
