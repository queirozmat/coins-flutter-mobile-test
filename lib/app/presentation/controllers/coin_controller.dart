import 'package:get/get.dart';
import 'package:moedas/app/domain/models/wallet_model.dart';
import 'package:moedas/app/domain/repository/coins_repository.dart';

class CoinController extends GetxController {
  final CoinsRepository coinsRepository = Get.put(CoinsRepository());
  var wallet = WalletModel().obs;

  Future<void> getWalletAndCoins() async {
    await coinsRepository
        .getWalletAndCoins()
        .then((value) => wallet.value = value);
  }
}
